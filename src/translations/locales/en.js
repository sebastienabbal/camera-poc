export default {
  global: {

  },
  alert: {
    error: "An error occured ..."
  },
  screens: {
    welcome: "Home",
    rules: "Rules",
    game: "Game"
  },
  welcome: {
    title: "Welcome POC Test Camera Tool"
  }
};
