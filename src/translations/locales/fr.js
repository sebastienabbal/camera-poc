export default {
  global: {

  },
  alert: {
    error: "Une erreur est survenue ..."
  },
  screens: {
    welcome: "Accueil",
    rules: "Règles",
    game: "Jeu"
  },
  welcome: {
    title: "Bienvenue sur le camera POC Tool"
  }
};
