export const userSetLocale = (locale) => ({
  type: 'SET_LOCALE',
  locale: locale
});
