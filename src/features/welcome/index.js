/*****************************************/
/*                IMPORTS                */
/*****************************************/
/* --------------- REACT --------------- */
import React, { useState, useEffect, useRef } from 'react';
import { Text, View, TouchableOpacity, Animated, Ease, Platform, NativeModules } from 'react-native';
import { connect } from 'react-redux';
import {color, style as mainStyle} from '../../styles/main';
import I18n from '../../translations/i18n';
import config from '../../api/constants';
import { alert } from '../../api/helper';
import { userSetLocale } from '../../actions/userActions';
import RNfs from 'react-native-fs';

import Loader from '../../components/_global/Loader';
import Button from '../../components/_global/Button';
import LocaleSelector from '../../components/locale/LocaleSelector';

const IndexScreen = (props) => {
  // Constants
  const loaderRef = useRef(null);
  const [initializing, setInitializing] = useState(true);
  const fadeContainerAnim = useRef(new Animated.Value(0)).current;

  // Hook effects
  useEffect(() => {
    initLocale();
    onAuth();
    onSTT();
  }, []);
  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      // Function()
    });

    return unsubscribe;
  }, []);

  // Functions
  const onSTT = () => {
    RNfs.readDirAssets("../../assets/audio/record.mp3")
    .then((result) => {
      console.log(result);
    })
    .catch(function (err) {
      console.log(err);
    })

  /*
    axios({
      method: 'post',
      url: "https://speech.googleapis.com/v1p1beta1/speech:longrunningrecognize",
      data: {
        audio: {
          content: "../../assets/audio/record.mp3"
        },
        config: {
          enableAutomaticPunctuation: true,
          encoding: "LINEAR16",
          languageCode: "fr-FR",
          model: "default"
        }
      }
    })
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (err) {
      console.log(err);
    })
    */
  }
  const onAuth = () => {
    if (initializing) {
      setTimeout(() => {
        loaderRef.current.fadeOut();
        setTimeout(() => {setInitializing(false)}, 500);
        fadeInContainer();
      }, 1000);
    }
  }
  const initLocale = () => {
    if (initializing) {
      if (props.locale) {
        I18n.locale = props.locale;
      } else {
        var locale = Platform.OS === 'ios'
        ? NativeModules.SettingsManager.settings.AppleLocale ||
          NativeModules.SettingsManager.settings.AppleLanguages[0]
        : NativeModules.I18nManager.localeIdentifier;

        locale = locale ? locale.split("_")[0] : config.defaultLocale;
        if (!config.locales.find((item) => item.value == locale)) {
          locale = config.defaultLocale;
        }
        I18n.locale = locale;
        props.reduxUserSetLocale(locale);
      }
    }
  }

  const onStart = () => {
    fadeOutContainer();
    props.navigation.push("Game");
  }

  const fadeInContainer = () => {
    Animated.timing(fadeContainerAnim, {
      toValue: 1,
      duration: 2000,
      delay: 500,
      useNativeDriver: true
    }).start();
  };
  const fadeOutContainer = () => {
    Animated.timing(fadeContainerAnim, {
      toValue: 0,
      duration: 250,
      delay: 0,
      useNativeDriver: true
    }).start(() => {
      fadeInContainer();
    });
  };

  // Renders
  if (initializing) {
    return (
      <View style={[mainStyle.container]}>
        <Loader ref={loaderRef}/>
      </View>
    )
  };
  return (
    <View style={{width: '100%', height: '100%'}}>
      <View style={[mainStyle.container, mainStyle.middleFull]}>
        <Animated.View style={[{opacity: fadeContainerAnim}]}>
          <View style={mainStyle.center}>
            <Text style={[mainStyle.h2, {textAlign: 'center'}]}>{I18n.t('welcome.title')}</Text>
            <Button style={[mainStyle.secondaryButton, {marginTop: 20}]} textStyle={{ fontSize: 20 }} onPress={() => props.navigation.push('Camera')} text={"CAMERA"}/>
          </View>
        </Animated.View>
      </View>
      <Animated.View style={{ position: 'absolute', bottom: '5%', right: 0, left: 0, alignItems: 'center', opacity: fadeContainerAnim }}>
        <LocaleSelector />
      </Animated.View>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    locale: state.userReducer.locale,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    reduxUserSetLocale: (locale) => dispatch(userSetLocale(locale))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(IndexScreen);
