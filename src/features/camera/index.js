/*****************************************/
/*                IMPORTS                */
/*****************************************/
/* --------------- REACT --------------- */
import React, { useState, useEffect, useRef } from 'react';
import { Text, View, TouchableOpacity, Animated, Ease, Platform, NativeModules } from 'react-native';
import { connect } from 'react-redux';
import {color, width, height, style as mainStyle} from '../../styles/main';
import {cameraStyle} from '../../styles/camera';
import I18n from '../../translations/i18n';
import config from '../../api/constants';
import { alert } from '../../api/helper';
import { RNCamera, FaceDetector } from 'react-native-camera';

import Loader from '../../components/_global/Loader';
import Timer from '../../components/camera/Timer';

var timer = null;
var time = 0;

const CameraScreen = (props) => {
  // Constants
  const loaderRef = useRef(null);
  const cameraRef = useRef(null);
  const timerRef = useRef(null);
  const [initializing, setInitializing] = useState(true);
  const [flashMode, setFlashMode] = useState(RNCamera.Constants.FlashMode.off);
  const [typeMode, setTypeMode] = useState(RNCamera.Constants.Type.back);
  const [timeClip, setTimeClip] = useState(time);
  const [records, setRecords] = useState({
    recordedData: [],
    recordedClipsTime: []
  });
  const [recording, setRecording] = useState(false);
  const fadeContainerAnim = useRef(new Animated.Value(0)).current;

  // Functions
  const onInit = () => {
    if (initializing) {
      setTimeout(() => {
        loaderRef.current.fadeOut();
        setTimeout(() => {setInitializing(false)}, 500);
        fadeInContainer();
      }, 1000);
    }
  }

  const validRecords = () => {
    props.navigation.push("Editor", { records: records });
  }

  const cameraRecord = async () => {
    let { recordedData, recordedClipsTime } = records;
    var totalClipsTime = 0;
    recordedClipsTime.forEach((recordedClipTime) => { totalClipsTime = totalClipsTime + recordedClipTime });
    timerRef.current.stop();

    if (totalClipsTime >= 55) { // 60 (max) - min d'une vidéo pour éviter d'avoir le messsage d'erreur
      alert("danger", "Maximum de vidéo atteint.");
    } else {
      try {
        if (cameraRef) {
          if (recording) {
            setRecording(false);
            cameraRef.current.stopRecording();
          } else {
            timerRef.current.init(3000, async () => {
              setRecording(true);
              cameraRef.current.recordAsync({
                width: width,
                height: height,
                quality: RNCamera.Constants.VideoQuality["1080p"],
                orientation: "portrait",
                fixOrientation: true,
                maxDuration: 60,
                targetBitrate: 1000 * 1000 * 40 // 5Mbps
              })
              .then(async (data) => {
                if (time <= 5) {
                  alert("danger", "Séquence trop courte (Au moins 5 secondes).");
                } else {
                  recordedData = recordedData.slice(0);
                  recordedData.push(data);

                  recordedClipsTime = recordedClipsTime.slice(0);
                  recordedClipsTime.push(time);

                  setRecords({
                    recordedData,
                    recordedClipsTime
                  })
                  time = 0;
                  setTimeClip(time);
                }
              })
              .catch((err) => {
                alert("danger", err.message);
              })
            })
          }
        } else {
          alert("danger", "No camera component found.");
        }
      } catch(err) {
        alert("danger", err.message);
      }
    }
  };

  const removeLastRecord = () => {
    let { recordedData, recordedClipsTime } = records;

    recordedData.pop();
    recordedClipsTime.pop();

    setRecords({
      recordedData,
      recordedClipsTime
    })
  }

  const cameraFlash = async () => {
    if (flashMode == RNCamera.Constants.FlashMode.off) {
      setFlashMode(RNCamera.Constants.FlashMode.torch);
    } else {
      setFlashMode(RNCamera.Constants.FlashMode.off);
    }
  };

  const cameraType = async () => {
    if (typeMode == RNCamera.Constants.Type.back) {
      setTypeMode(RNCamera.Constants.Type.front);
    } else {
      setTypeMode(RNCamera.Constants.Type.back);
    }
  };

  const fadeInContainer = () => {
    Animated.timing(fadeContainerAnim, {
      toValue: 1,
      duration: 2000,
      delay: 500,
      useNativeDriver: true
    }).start();
  };
  const fadeOutContainer = () => {
    Animated.timing(fadeContainerAnim, {
      toValue: 0,
      duration: 250,
      delay: 0,
      useNativeDriver: true
    }).start(() => {
      fadeInContainer();
    });
  };

  // Renders
  return (
    <View style={cameraStyle.container}>
      <RNCamera
        ref={cameraRef}
        style={cameraStyle.preview}
        type={typeMode}
        flashMode={flashMode}
        onCameraReady={() => {
          onInit();
        }}
        onRecordingStart={() => {
          let { recordedClipsTime } = records;
          var totalClipsTime = 0;

          recordedClipsTime.forEach((recordedClipTime) => { totalClipsTime = totalClipsTime + recordedClipTime });
          timer = setInterval(() => {
            if (totalClipsTime + time >= 60) {
              setRecording(false);
              cameraRef.current.stopRecording();
            } else {
              time = time + 1;
              setTimeClip(time);
            }
          }, 1000);
        }}
        onRecordingEnd={() => {
          clearInterval(timer);
          timer = null;
        }}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        androidRecordAudioPermissionOptions={{
          title: 'Permission to use audio recording',
          message: 'We need your permission to use your audio',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
      >
        {initializing ? (
          <View style={[mainStyle.container, mainStyle.middleFull, { width: '100%', height: '100%' }]}>
            <View style={{ width: '100%', height: '100%' }}>
              <Loader ref={loaderRef}/>
            </View>
          </View>
        ) : (
          <View style={[{ width: '100%', height: '100%' }]}>
            <View style={{ flexDirection: 'row' }}>
              {records.recordedClipsTime.length > 0 && (
                records.recordedClipsTime.map((clip, index) => {
                  const newWidth = (width - 2 - (records.recordedClipsTime.length * 4)) * (clip/60);
                  return (
                    <View key={index} style={{ height: 10, width: newWidth, backgroundColor: color.warning, marginHorizontal: 2 }} />
                  )
                })
              )}
              {recording && (
                <View style={{ height: 10, width: (width - 8 - (records.recordedClipsTime.length * 4)) * ((timeClip)/60), backgroundColor: color.success, marginHorizontal: 2 }} />
              )}
            </View>

            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
              <Timer ref={timerRef} />
            </View>

            <View style={{ position: 'absolute', top: '3%', right: '3%' }}>
              <TouchableOpacity onPress={() => cameraFlash()} style={cameraStyle.capture}>
                <Text style={{ fontSize: 14, fontWeight: 'bold' }}> F </Text>
              </TouchableOpacity>
            </View>
            <View style={{ position: 'absolute', top: '13%', right: '3%' }}>
              <TouchableOpacity onPress={() => cameraType()} style={cameraStyle.capture}>
                <Text style={{ fontSize: 14, fontWeight: 'bold' }}> R </Text>
              </TouchableOpacity>
            </View>
            {!recording && (records.recordedClipsTime.length > 0) && (
              <View style={{ position: 'absolute', top: '3%', left: '3%' }}>
                <TouchableOpacity onPress={() => removeLastRecord()} style={cameraStyle.capture}>
                  <Text style={{ fontSize: 14, fontWeight: 'bold' }}> X </Text>
                </TouchableOpacity>
              </View>
            )}
            <View style={{ position: 'absolute', left: 0, right: 0, bottom: 20 }}>
              <TouchableOpacity onPress={() => cameraRecord()} style={[cameraStyle.capture, { backgroundColor: recording ? color.success : 'white' }]}>
                <Text style={{ fontSize: 14, fontWeight: 'bold' }}> RECORD </Text>
              </TouchableOpacity>
            </View>
            {records.recordedClipsTime.length > 0 && !recording && (
              <View style={{ position: 'absolute', right: 0, bottom: 20 }}>
                <TouchableOpacity onPress={() => validRecords()} style={[cameraStyle.capture]}>
                  <Text style={{ fontSize: 14, fontWeight: 'bold' }}> VALID </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        )}
      </RNCamera>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    locale: state.userReducer.locale,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    reduxUserSetLocale: (locale) => dispatch(userSetLocale(locale))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CameraScreen);
