/*****************************************/
/*                IMPORTS                */
/*****************************************/
/* --------------- REACT --------------- */
import React, { useState, useEffect, useRef } from 'react';
import { Text, View, TouchableOpacity, TouchableWithoutFeedback, Animated, Ease, Platform, NativeModules } from 'react-native';
import { connect } from 'react-redux';
import {color, width, height, style as mainStyle} from '../../styles/main';
import {cameraStyle} from '../../styles/camera';
import I18n from '../../translations/i18n';
import config from '../../api/constants';
import { alert, getRandomUniqueId } from '../../api/helper';
import Video from 'react-native-video';
import { LogLevel, RNFFmpeg, RNFFmpegConfig } from 'react-native-ffmpeg';

import Loader from '../../components/_global/Loader';
import Timer from '../../components/camera/Timer';

var timer = null;
var time = 0;

const PreviewScreen = (props) => {
  // Constants
  const loaderRef = useRef(null);
  const playerRef = useRef(null);
  const [initializing, setInitializing] = useState(true);
  const [playerIsMuted, setPlayerIsMuted] = useState(false);
  const [playerIsPaused, setPlayerIsPaused] = useState(false);
  const fadeContainerAnim = useRef(new Animated.Value(0)).current;

  // Hook effects
  useEffect(() => {
    onInit();
  }, []);

  // Functions
  const onInit = () => {
    if (initializing) {
      setTimeout(() => {
        loaderRef.current.fadeOut();
        setTimeout(() => {
          setInitializing(false);
        }, 500);
        fadeInContainer();
      }, 1000);
    }
  }

  const fadeInContainer = () => {
    Animated.timing(fadeContainerAnim, {
      toValue: 1,
      duration: 2000,
      delay: 500,
      useNativeDriver: true
    }).start();
  };
  const fadeOutContainer = () => {
    Animated.timing(fadeContainerAnim, {
      toValue: 0,
      duration: 250,
      delay: 0,
      useNativeDriver: true
    }).start(() => {
      fadeInContainer();
    });
  };

  const onPause = () => {
    setPlayerIsPaused(!playerIsPaused);
  }
  const onMute = () => {
    setPlayerIsMuted(!playerIsMuted);
  }

  // Renders
  if (initializing) {
    return (
      <View style={[mainStyle.container]}>
        <Loader ref={loaderRef}/>
      </View>
    )
  };
  return (
    <View style={{ width: '100%', height: '100%' }}>
      <TouchableWithoutFeedback onPress={() => onPause()}>
        <View style={{ width: '100%', height: '100%' }}>
          <Video source={{uri: props.route.params.videoUri}}
            ref={playerRef}
            style={cameraStyle.previewContainer}
            paused={playerIsPaused}
            muted={playerIsMuted}
            posterResizeMode={"cover"}
            repeat={true}
          />
          <View style={{ position: 'absolute', right: 0, top: '3%' }}>
            <TouchableOpacity onPress={() => onMute()} style={[cameraStyle.capture, {backgroundColor: playerIsMuted ? color.danger : color.success}]}>
              <Text style={{ fontSize: 14, fontWeight: 'bold' }}>SOUND</Text>
            </TouchableOpacity>
          </View>
          <View style={{ position: 'absolute', left: 0, bottom: 50 }}>
            <TouchableOpacity onPress={() => props.navigation.goBack()} style={cameraStyle.capture}>
              <Text style={{ fontSize: 14, fontWeight: 'bold' }}>BACK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    locale: state.userReducer.locale,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    reduxUserSetLocale: (locale) => dispatch(userSetLocale(locale))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PreviewScreen);
