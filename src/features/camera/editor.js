/*****************************************/
/*                IMPORTS                */
/*****************************************/
/* --------------- REACT --------------- */
import React, { useState, useEffect, useRef } from 'react';
import { Text, View, TouchableOpacity, TouchableWithoutFeedback, Animated, Ease, Platform, NativeModules } from 'react-native';
import { connect } from 'react-redux';
import {color, width, height, style as mainStyle} from '../../styles/main';
import {cameraStyle} from '../../styles/camera';
import I18n from '../../translations/i18n';
import config from '../../api/constants';
import { alert, getRandomUniqueId } from '../../api/helper';
import Video from 'react-native-video';
import { LogLevel, RNFFmpeg, RNFFmpegConfig } from 'react-native-ffmpeg';
import axios from 'axios';

import Loader from '../../components/_global/Loader';
import Timer from '../../components/camera/Timer';

var timer = null;
var time = 0;

const EditorScreen = (props) => {
  // Constants
  const loaderRef = useRef(null);
  const playerRef = useRef(null);
  const timerRef = useRef(null);
  const [initializing, setInitializing] = useState(true);
  const [converting, setConverting] = useState(false);
  const [converterProgress, setConverterProgress] = useState(0);
  const [playerIndex, setPlayerIndex] = useState(0);
  const [playerIsEnd, setPlayerIsEnd] = useState(false);
  const [playerIsMuted, setPlayerIsMuted] = useState(false);
  const [playerIsPaused, setPlayerIsPaused] = useState(false);
  const [records, setRecords] = useState(props.route.params.records);
  const fadeContainerAnim = useRef(new Animated.Value(0)).current;

  // Hook effects
  useEffect(() => {
    onInit();
    RNFFmpegConfig.enableStatisticsCallback(onConvertProgress);
    RNFFmpegConfig.disableLogs();
  }, []);

  // Functions
  const onInit = () => {
    if (initializing) {
      setTimeout(() => {
        loaderRef.current.fadeOut();
        setTimeout(() => {
          setInitializing(false);
        }, 500);
        fadeInContainer();
      }, 1000);
    }
  }

  const fadeInContainer = () => {
    Animated.timing(fadeContainerAnim, {
      toValue: 1,
      duration: 2000,
      delay: 500,
      useNativeDriver: true
    }).start();
  };
  const fadeOutContainer = () => {
    Animated.timing(fadeContainerAnim, {
      toValue: 0,
      duration: 250,
      delay: 0,
      useNativeDriver: true
    }).start(() => {
      fadeInContainer();
    });
  };

  const playerNextVideo = (recoredData) => {
    if ((playerIndex + 1) < records.recordedData.length) {
      setPlayerIndex(playerIndex + 1);
    } else {
      setPlayerIsEnd(true);
    }
  }
  const playerGoToVideo = (index) => {
    setPlayerIsPaused(false);
    setPlayerIndex(index);
    setPlayerIsEnd(false);
    playerRef.current.seek(0);
  }
  const onPlayerRestart = () => {
    setPlayerIsPaused(false);
    playerRef.current.seek(0);
    setPlayerIndex(0);
    setPlayerIsEnd(false);
  }
  const onPause = () => {
    if (playerIsEnd) {
      onPlayerRestart();
    } else {
      setPlayerIsPaused(!playerIsPaused);
    }
  }
  const onMute = () => {
    setPlayerIsMuted(!playerIsMuted);
  }
  const onDone = () => {
    onConvertStart();
  }

  const onConvertStart = () => {
    const converterId = getRandomUniqueId();
    const path = (records.recordedData[0].uri).substring(0, (records.recordedData[0].uri).lastIndexOf("/"));

    setConverting(true);
    setPlayerIsPaused(true);

    setTimeout(() => {
      if (records.recordedData.length > 1) {
        console.log("Convert init (multiple video sequences)");
        mergeVideos(path, converterId, (err, result) => {
          if (!err) {
            console.log("Merge done");
            setConverting(false);
            setConverterProgress(0);
            alert("success", "Process done succesfully !");
            onConvertEnd(result);
          } else {
            alert("danger", err);
          }
        });
      } else {
        console.log("Convert init (solo video sequence)");
        setConverting(false);
        setConverterProgress(0);
        alert("success", "Process done succesfully !");
        onConvertEnd(records.recordedData[0].uri);
      }
    }, 1000);
  }

  const extractAudioToSTT = (path, converterId, callback) => {
    const input = path + '/' + converterId + '_merged.mp4';
    const output = path + '/' + converterId + '_audio.aac';

    var request = '-i ' + input + ' -vn -acodec copy ' + output;

    RNFFmpeg.execute(request)
    .then((result) => {
      axios({
        method: 'post',
        url: "https://speech.googleapis.com/v1p1beta1/speech:longrunningrecognize",
        data: {
          audio: {
            content: output
          },
          config: {
            enableAutomaticPunctuation: true,
            encoding: "LINEAR16",
            languageCode: "fr-FR",
            model: "default"
          }
        }
      })
      .then(function (response) {
        console.log(response.data);
        callback(null, response.data);
      })
      .catch(function (err) {
        callback(err, null);
        console.log(err);
        alert("danger", err.data ? (err.data.message) : (err.message ? err.message : err));
        setConverting(false);
      })
    })
    .catch((err) => {
      callback(err, null);
      alert("danger", err.message ? err.message : err);
      setConverting(false);
    })
  }

  const mergeVideos = (path, converterId, callback) => {
    var inputs = "";
    var filters = "";
    const countVideos = records.recordedData.length;
    const output = path + '/' + converterId + '_merged.mp4';
    var requestInputs = "";

    records.recordedData.forEach((input, index) => {
      inputs = inputs + " -i " + input.uri;
      filters = filters + "[" + index + ":v:0][" + index + ":a:0]"
    });

    var request = '-y' + inputs + ' -r 24000/1001 -filter_complex "' + filters + 'concat=n=' + countVideos + ':v=1:a=1 [out]" -map "[out]" ' + output;

    RNFFmpeg.execute(request)
    .then((result) => {
      callback(null, output);
    })
    .catch((err) => {
      callback(err, null);
      alert("danger", err.message ? err.message : err);
      setConverting(false);
    })
  }

  const onConvertProgress = (statistics) => {
    if (statistics) {
      let timeInMilliseconds = statistics.time;
      if (timeInMilliseconds > 0) {
        let totalVideoDuration = records.recordedClipsTime[playerIndex];
        let completePercentage = Math.round((timeInMilliseconds * 100) / (totalVideoDuration * 1000));
        if (completePercentage >= 100) {
          completePercentage = 100;
        }
        setConverterProgress(completePercentage);
      }
    }
  }

  const onConvertEnd = (videoUri) => {
    props.navigation.push("Preview", { videoUri: videoUri });
  }

  // Renders
  if (initializing) {
    return (
      <View style={[mainStyle.container]}>
        <Loader ref={loaderRef}/>
      </View>
    )
  };
  return (
    <View style={{ width: '100%', height: '100%' }}>
      {converting && (
        <View style={{ position: 'absolute', left: 0, top: 0, width: width, height: height, zIndex: 9999 }}>
          <Loader ref={loaderRef}/>
          <View style={{position: 'absolute', left: 0, right: 0, bottom: 0, top: 150, alignItems: 'center', justifyContent: 'center'}}>
            <Text style={[mainStyle.h2, {fontWeight: 'bold'}]}>... CONVERTING ...</Text>
            <Text style={[mainStyle.h2, {fontWeight: 'bold'}]}>{ converterProgress }%</Text>
          </View>
        </View>
      )}

      <View style={{ flexDirection: 'row', position: 'absolute', top: 0, left: 0, zIndex: 1 }}>
        {records.recordedClipsTime.length > 0 && (
          records.recordedClipsTime.map((clip, index) => {
            const newWidth = (width - 2 - (records.recordedClipsTime.length * 4)) * (clip/60);
            return (
              <TouchableOpacity key={index} onPress={() => playerGoToVideo(index)}>
                <View style={{ height: 25, width: newWidth, marginHorizontal: 2 }}>
                  <View style={{ width: '100%', height: '100%', backgroundColor: 'red' }} />
                </View>
              </TouchableOpacity>
            )
          })
        )}
      </View>
      <TouchableWithoutFeedback onPress={() => onPause()}>
        <View style={{ width: '100%', height: '100%' }}>
          <Video source={{uri: records.recordedData[playerIndex].uri}}
            ref={playerRef}
            style={cameraStyle.previewContainer}
            paused={playerIsPaused}
            muted={playerIsMuted}
            posterResizeMode={"cover"}
            onEnd={() => {
              playerNextVideo();
            }}
          />
          <View style={{ position: 'absolute', right: 0, top: '3%' }}>
            <TouchableOpacity onPress={() => onMute()} style={[cameraStyle.capture, {backgroundColor: playerIsMuted ? color.danger : color.success}]}>
              <Text style={{ fontSize: 14, fontWeight: 'bold' }}>SOUND</Text>
            </TouchableOpacity>
          </View>
          <View style={{ position: 'absolute', left: 0, bottom: 50 }}>
            <TouchableOpacity onPress={() => props.navigation.goBack()} style={cameraStyle.capture}>
              <Text style={{ fontSize: 14, fontWeight: 'bold' }}>BACK</Text>
            </TouchableOpacity>
          </View>
          {playerIsEnd && (
            <View style={{ position: 'absolute', left: 0, right: 0, bottom: 50 }}>
              <TouchableOpacity onPress={() => onPlayerRestart()} style={[cameraStyle.capture]}>
                <Text style={{ fontSize: 14, fontWeight: 'bold' }}> RESTART </Text>
              </TouchableOpacity>
            </View>
          )}
          <View style={{ position: 'absolute', right: 0, bottom: 50 }}>
            <TouchableOpacity onPress={() => onDone()} style={cameraStyle.capture}>
              <Text style={{ fontSize: 14, fontWeight: 'bold' }}>DONE</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    locale: state.userReducer.locale,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    reduxUserSetLocale: (locale) => dispatch(userSetLocale(locale))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditorScreen);
