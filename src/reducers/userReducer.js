const initialState = {
  locale: null
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_LOCALE': {
      return {
        ...state,
        locale: action.locale
      }
    }
    default: {
      return state;
    }
  }
};

export default userReducer;
