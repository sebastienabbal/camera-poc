import { showMessage, hideMessage } from "react-native-flash-message";
import {colors as color} from '../styles/colors';

function alert (type, message) {
  showMessage({
    message: message.toString(),
    type: "success",
    icon: "auto",
    backgroundColor: color[type],
    color: "white"
  });
}

function getRandom(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomUniqueId() {
  return Math.random().toString(36).substr(2, 9);
}

export { alert, getRandom, getRandomUniqueId }
