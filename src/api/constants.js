export default {
  google: {
    client_id: "1012414146141-4gqkp69tso2qvjeeqf33unpi3m8plvbg.apps.googleusercontent.com",
    reversed_client_id: "com.googleusercontent.apps.1012414146141-gha3lvo6ascd4i8enlujjm0nbjdnldsl"
  },
  defaultLocale: "en",
  locales: [
    {
      value: "fr",
      label: "Français",
      flag: require("../assets/image/locale/fr.png")
    },
    {
      value: "en",
      label: "English",
      flag: require("../assets/image/locale/en.png")
    }
  ]
}
