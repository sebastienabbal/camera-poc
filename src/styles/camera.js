import { Platform } from 'react-native';
import {width, height} from './main';
import {colors as color} from './colors';

const cameraStyle = {
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  previewContainer: {
    backgroundColor: 'black',
    width: width,
    height: height
  }
};

export {cameraStyle};
