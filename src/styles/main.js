import { getStatusBarHeight } from 'react-native-status-bar-height';
import {colors as color} from './colors';
import { Dimensions } from 'react-native';

const statusBarHeight = getStatusBarHeight(true);
const {width, height} = Dimensions.get('window');

const style = {
  background: {
    backgroundColor: color.backgroundContainer,
    width: '100%',
    height: '100%'
  },
  container: {
    backgroundColor: color.backgroundContainer,
    flex: 1,
    paddingTop: statusBarHeight,
    paddingBottom: 10,
    paddingHorizontal: '8%'
  },
  header: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    borderBottomWidth: 1,
    borderColor: 'white'
  },
  middle: {
    justifyContent: 'center'
  },
  center: {
    alignItems: 'center'
  },
  middleFull: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  h1: {
    fontSize: 50,
    color: color.textColor
  },
  h2: {
    fontSize: 30,
    color: color.textColor
  },
  h3: {
    fontSize: 20,
    color: color.textColor
  },
  p: {
    fontSize: 18,
    color: color.textColor
  },
  bold: {
    fontSize: 18,
    color: color.textColor,
    fontWeight: 'bold'
  },
  primaryButton: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderWidth: 2,
    borderColor: color.primary,
    backgroundColor: color.primary,
    borderRadius: 5
  },
  secondaryButton: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderWidth: 2,
    borderColor: color.secondary,
    backgroundColor: color.secondary,
    borderRadius: 5
  }
}

export {style, color, width, height};
