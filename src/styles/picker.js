import { Platform } from 'react-native';
import {width, height} from './main';
import {colors as color} from './colors';

const pickerStyle = {
  container: {
    borderColor: 'grey',
    borderWidth: 1,
    padding: 15,
    height: height/2
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'stretch'
  },
  text: {
    fontSize: 18
  },
  headerFooterContainer: {
    padding: 10,
    alignItems: 'center'
  },
  clearButton: { backgroundColor: 'grey', borderRadius: 5, marginRight: 10, padding: 5 },
  optionContainer: {
    padding: 10,
    borderBottomColor: 'white',
    borderBottomWidth: 1
  },
  welcomeBackdrop: {
    backgroundColor: 'black',
    opacity: 1,
    minHeight: Platform.OS === 'ios' ? height : height+30,
  },
  backdrop: {
    backgroundColor: 'black',
    opacity: 1
  },
  optionInnerContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  box: {
    width: 20,
    height: 20,
    marginRight: 10
  }
};

export {pickerStyle};
