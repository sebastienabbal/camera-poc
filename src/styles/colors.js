const colors = {
  backgroundContainer: "#101033",
  backgroundStatusBar: "#09091c",
  textColor: "white",
  primary: "#f59c0d",
  secondary: "#7686FF",
  success: "#22a12c",
  warning: "#f59c0d",
  danger: "#c40000",
  info: "#0058c4"
}

export {colors};
