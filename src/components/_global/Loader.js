/*****************************************/
/*                IMPORTS                */
/*****************************************/
/* --------------- REACT --------------- */
import React, { useRef, useImperativeHandle, forwardRef } from 'react';
import { View, Animated } from 'react-native';
import LottieView from 'lottie-react-native';

function Loader(props, ref) {
  const componentRef = useRef();
  const fadeAnim = useRef(new Animated.Value(1)).current;

  useImperativeHandle(ref, () => ({
    fadeOut: () => {
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true
      }).start();
    }
  }));

  return (
    <Animated.View style={{ opacity: fadeAnim, width: '100%', height: '100%', position: 'relative', bottom: '10%' }}>
      <LottieView source={require('../../assets/lottie/loader.json')} autoPlay={true} loop={true} />
    </Animated.View>
  );
}

export default forwardRef(Loader);
