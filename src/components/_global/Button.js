import React, { useRef, useImperativeHandle, forwardRef } from 'react';
import { Text, View, TouchableOpacity, Animated, Ease } from 'react-native';
import {color, style as mainStyle} from '../../styles/main';

function Button(props, ref) {
  const componentRef = useRef();
  const fadeAnim = useRef(new Animated.Value(1)).current;

  useImperativeHandle(ref, () => ({
    fadeIn: () => {
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      }).start();
    },
    fadeOut: () => {
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true
      }).start();
    }
  }));

  return (
    <Animated.View style={{ opacity: fadeAnim }}>
      <TouchableOpacity style={[mainStyle.primaryButton, props.style ? props.style : {}]} onPress={props.onPress}>
        <Text style={[mainStyle.h2, props.textStyle ? props.textStyle : {}]}>{props.text}</Text>
      </TouchableOpacity>
    </Animated.View>
  );
}

export default forwardRef(Button);
