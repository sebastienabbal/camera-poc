/*****************************************/
/*                IMPORTS                */
/*****************************************/
/* --------------- REACT --------------- */
import React, { useState, useEffect, useRef, useImperativeHandle, forwardRef } from 'react';
import { Alert, View, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { CustomPicker } from 'react-native-custom-picker';
import I18n from '../../translations/i18n';
import config from '../../api/constants';

import { userSetLocale } from '../../actions/userActions';

import {color, style as mainStyle} from '../../styles/main';
import { pickerStyle } from '../../styles/picker';

function LocaleSelector(props, ref) {
  const componentRef = useRef();

  // Hook effects
  useEffect(() => {
    props.reduxUserSetLocale(props.locale ? props.locale : "en");
  }, []);

  const options = config.locales;
  const renderField = (settings) => {
    const { selectedItem, defaultText, getLabel } = settings;
    return (
      <View style={[{flexDirection: 'row'}]}>
        <Image source={selectedItem.flag} style={{width:32, height:32}}/>
        <Text style={[mainStyle.h3, {marginLeft: 10, paddingTop:3}]}>{getLabel(selectedItem)}</Text>
      </View>
    )
  }
  const renderOption = (settings) => {
    const { item, getLabel } = settings;
    return (
      <View style={pickerStyle.optionContainer}>
        <View style={pickerStyle.innerContainer}>
          <Image source={item.flag} style={[pickerStyle.box, {width:32, height:32}]}/>
          <Text style={[mainStyle.p, { color: 'black', marginLeft: 10, paddingTop: 5 }]}>{getLabel(item)}</Text>
        </View>
      </View>
    )
  }

  return (
    <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
      <CustomPicker
        defaultValue={options.find((item) => item.value == props.locale)}
        placeholder={'Select language'}
        options={options}
        getLabel={item => item.label}
        fieldTemplate={renderField}
        optionTemplate={renderOption}
        onValueChange={item => {
          props.reduxUserSetLocale(item.value);
          I18n.locale = item.value;
        }}
      />
    </View>
  );
}

const mapStateToProps = (state) => {
  return {
    locale: state.userReducer.locale,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    reduxUserSetLocale: (locale) => dispatch(userSetLocale(locale))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(forwardRef(LocaleSelector));
