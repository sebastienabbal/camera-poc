import React, { useRef, useState, useImperativeHandle, forwardRef } from 'react';
import { Text, View, TouchableOpacity, Animated, Ease } from 'react-native';
import {color, style as mainStyle} from '../../styles/main';

var timer = null;

function Timer(props, ref) {
  const componentRef = useRef();
  const fadeAnim = useRef(new Animated.Value(1)).current;

  const [stateTime, setStateTime] = useState(0);
  const scaleTimeAnim = useRef(new Animated.Value(1)).current;
  const fadeTimeAnim = useRef(new Animated.Value(0)).current;

  useImperativeHandle(ref, () => ({
    init: (time, callback) => {
      initTimer(time, callback);
    },
    stop: () => {
      stopTimer();
    }
  }));

  const initTimer = (time, callback) => {
    const i = 1000;
    time = time + 1000;

    fadeInTime();
    timer = setInterval(() => {
      if (time > 0) {
        time = time - i;
        setStateTime(time/1000);

        if (time <= 20000 && time > 5000) {
          growScaleTime(1 + (1000/time));
        } else if (time <= 5000 && time > 0) {
          growScaleTime(((6 - (time/1000))/10) + 1 + (1000/time));
        } else {
          ungrowScaleTime();
          fadeOutTime();
        }
      } else {
        callback();
        stopTimer();
      }
    }, i);
  }
  const stopTimer = () => {
    fadeOutTime();
    clearInterval(timer);
    timer = null;
    setStateTime(null);
  }

  const growScaleTime = (val) => {
    Animated.timing(scaleTimeAnim, {
      toValue: val,
      duration: 500,
      useNativeDriver: true
    }).start();
  }
  const ungrowScaleTime = () => {
    Animated.timing(scaleTimeAnim, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true
    }).start();
  }
  const fadeInTime = () => {
    Animated.timing(fadeTimeAnim, {
      toValue: 1,
      duration: 500,
      delay: 1000,
      useNativeDriver: true
    }).start();
  }
  const fadeOutTime = () => {
    Animated.timing(fadeTimeAnim, {
      toValue: 0,
      duration: 500,
      delay: 0,
      useNativeDriver: true
    }).start();
  }

  return (
    stateTime > 0 ? (
      <Animated.Text style={[mainStyle.bold, mainStyle.h1, {position: 'absolute', bottom: '25%', left: 0, right: 0, opacity: fadeTimeAnim, textAlign: 'center', transform: [{ scale: scaleTimeAnim }]}]}>{stateTime}</Animated.Text>
    ) : null
  );
}

export default forwardRef(Timer);
