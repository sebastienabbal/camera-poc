/* React */
import React, { useState, useEffect } from 'react';
import { View, StatusBar, LogBox } from 'react-native';
import {color, style as mainStyle} from './src/styles/main';
import FlashMessage from "react-native-flash-message";

/* Redux */
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { store, persistor } from './src/store';

/* Navigation */
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const navigationOptions = {
  headerShown: false
}

/* Screens */
import WelcomeScreen from './src/features/welcome/index';
import CameraScreen from './src/features/camera/index';
import EditorScreen from './src/features/camera/editor';
import PreviewScreen from './src/features/camera/preview';

/* Core App */
const App = () => {
  LogBox.ignoreLogs(['Warning: componentWillReceiveProps']);

  return (
    <View style={mainStyle.background}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer>
            <StatusBar backgroundColor={color.backgroundStatusBar} barStyle="light-content" />
            <Stack.Navigator>
              <Stack.Screen name="Welcome" component={WelcomeScreen} headerMode={"none"} options={navigationOptions} />
              <Stack.Screen name="Camera" component={CameraScreen} options={navigationOptions} />
              <Stack.Screen name="Editor" component={EditorScreen} options={navigationOptions} />
              <Stack.Screen name="Preview" component={PreviewScreen} options={navigationOptions} />
            </Stack.Navigator>
          </NavigationContainer>
        </PersistGate>
      </Provider>
      <FlashMessage position="top" />
    </View>
  );
}

export default App;
